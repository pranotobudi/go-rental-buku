module github.com/pranotobudi/Go-Rental-Buku

go 1.16

require (
	github.com/bxcodec/faker/v3 v3.6.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-gonic/gin v1.7.1
	github.com/google/uuid v1.2.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/thanhpk/randstr v1.0.4 // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.7
)
